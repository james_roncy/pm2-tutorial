const express = require('express')
const app = express()
var env = require('node-env-file')
app.get('/', (req, res) => {
    var loadedCon = process.env.NODE_ENV || 'staging'
    env(__dirname+ '/.env-' + loadedCon)
    var mysqlDetails = {
        host: process.env.MYSQL_HOST,
        pwd: process.env.MYSQL_PWD,
        user: process.env.MYSQL_USER,
        test: 'test'
    }
   res.send(mysqlDetails)
})

app.listen(8080, () => console.log('Example app listening on port 3000!'))
