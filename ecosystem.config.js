module.exports = {
  apps : [{
    name   : "express",
    script : "./app.js",
    instances: "2",
    watch       : true,
    env: {
      "NODE_ENV": "staging",
    },
    env_production : {
       "NODE_ENV": "production"
    }
  }]
}